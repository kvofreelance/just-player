package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import jp.co.kayo.android.localplayer.AlbumartActivity;
import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TagEditActivity;
import jp.co.kayo.android.localplayer.adapter.PlaybackListViewAdapter;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.FastOnTouchListener;
import jp.co.kayo.android.localplayer.core.IProgressView;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MultipleChoiceMediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.ImageObserver;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.FavoriteInfo;
import jp.co.kayo.android.localplayer.util.bean.MediaData;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

@TargetApi(11)
public class PlaybackListViewFragment extends BaseListFragment implements
        ContentManager, ContextMenuFragment, LoaderCallbacks<Cursor>,
        OnScrollListener, IProgressView, android.view.View.OnClickListener,
        OnLongClickListener {

    MyPreferenceManager mPref;
    ListView mListView;
    PlaybackListViewAdapter mAdapter;
    private ViewCache mViewcache;
    Runnable mTask = null;
    private AnActionModeOfEpicProportions mActionMode;
    Bitmap sourcebmp;
    ImageView imageAlbumArt;
    TextView txtTitle1;
    TextView txtTitle2;
    TextView txtTitle3;
    RatingBar ratingBar1;
    int imagesize;
    private LinearLayout mPlayViewLayout;
    private LinearLayout mSongInfoLayout;
    private String mOrgTitle;
    private String mOrgArtist;
    private String mOrgAlbum;

    private Handler myHandler = new UpdateMyHandler(this);

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = new MyPreferenceManager(getActivity());
        mViewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.playorder_grid_view, container,
                false);

        imageAlbumArt = (ImageView) root.findViewById(R.id.imageAlbumArt);
        txtTitle1 = (TextView) root.findViewById(R.id.txtTitle1);
        txtTitle2 = (TextView) root.findViewById(R.id.txtTitle2);
        txtTitle3 = (TextView) root.findViewById(R.id.txtTitle3);
        ratingBar1 = (RatingBar) root.findViewById(R.id.ratingBar1);

        imageAlbumArt.setOnClickListener(this);
        imageAlbumArt.setOnLongClickListener(this);

        mListView = (ListView) root.findViewById(android.R.id.list);
        mListView.setOnScrollListener(this);
        mActionMode = new AnActionModeOfEpicProportions(getActivity(),
                mListView, mHandler);

        // 画面が縦向きならアルバムアートを拡大するためにアルバムアートレイアウトを取得
        Configuration config = getResources().getConfiguration();
        if (config.orientation == Configuration.ORIENTATION_PORTRAIT
                || getFragmentManager().findFragmentByTag(
                        SystemConsts.TAG_EQUALIZER) != null) {
            mPlayViewLayout = (LinearLayout) root
                    .findViewById(R.id.playViewLayout);
            mPlayViewLayout.setOnClickListener(this);
            mSongInfoLayout = (LinearLayout) root
                    .findViewById(R.id.songInfoLayout);
            txtTitle1.setTextSize(getResources().getDimension(
                    R.dimen.scaleDownFontSize2));
            txtTitle2.setTextSize(getResources().getDimension(
                    R.dimen.scaleDownFontSize2));
            txtTitle3.setTextSize(getResources().getDimension(
                    R.dimen.scaleDownFontSize2));
        }

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.imagesize = getResources().getDimensionPixelSize(
                R.dimen.albumart_size);
        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.d("PlayOrder.onResume");
        if (getView() != null) {
            imageAlbumArt = (ImageView) getView().findViewById(
                    R.id.imageAlbumArt);
            setAlbumImage();
            if (mAdapter != null) {
                focusPlayItem();
            }
        }

        // ヘッダーテキストの色を設定
        mViewcache.getColorset().setColor(ColorSet.KEY_DEFAULT_PRI_COLOR,
                txtTitle1);
        mViewcache.getColorset().setColor(ColorSet.KEY_DEFAULT_SEC_COLOR,
                txtTitle2);

    }

    @Override
    public void onPause() {
        // アルバムアートを縮小処理のタイマーが動いていたら停止
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (imageAlbumArt != null) {
            imageAlbumArt.setImageBitmap(null);
            imageAlbumArt = null;
        }
        if (sourcebmp != null) {
            sourcebmp.recycle();
            sourcebmp = null;
        }
        super.onPause();
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
        if (mAdapter != null) {
            setAlbumImage();
        }
    }

    @Override
    public void changedMedia() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
            setAlbumImage();
            focusPlayItem();
        }
    }

    private void focusPlayItem() {
        IMediaPlayerService binder = getBinder();
        if (mPref.isKeepPlaybackPosition() && binder != null) {
            int position;
            try {
                position = binder.getPosition();
                if (isVisible()) {
                    ListView listview = getListView();
                    int firstpos = listview.getFirstVisiblePosition();
                    int lastpos = listview.getLastVisiblePosition();
                    if (position < firstpos || position > lastpos) {
                        listview.setSelectionFromTop(position, 0);
                    }
                }
            } catch (RemoteException e) {
            }
        }
    }

    private final class AnActionModeOfEpicProportions extends
            MultipleChoiceMediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                ListView listView, Handler handler) {
            super(context, listView, handler);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_remove))
                    .setIcon(R.drawable.ic_menu_delete)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_selectall))
                    .setIcon(R.drawable.ic_menu_selectall)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.txt_web_more))
                    .setIcon(R.drawable.ic_menu_wikipedia)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_rating))
                    .setIcon(R.drawable.ic_menu_star)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_artist))
                    .setIcon(R.drawable.ic_menu_btn_artist)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_album))
                    .setIcon(R.drawable.ic_menu_album)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            boolean b = mPref.useLastFM();
            if (b) {
                menu.add(getString(R.string.sub_mnu_love)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
                menu.add(getString(R.string.sub_mnu_ban)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }

            // 文字化け修正メニューを追加
            menu.add(getString(R.string.sub_mnu_edit))
                    .setIcon(R.drawable.ic_menu_strenc)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_refresh)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
            return true;
        }
    };

    private final String[] FETCH = new String[] { AudioMedia._ID,
            AudioMedia.TITLE, AudioMedia.ARTIST, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY, AudioMedia.DATA, AudioMedia.DURATION };

    private MediaData loadMediaItem(long id) {
        if (getActivity() != null) {
            Cursor cursor = null;
            try {
                cursor = getActivity().getContentResolver().query(
                        ContentUris.withAppendedId(
                                MediaConsts.MEDIA_CONTENT_URI, id), FETCH,
                        null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    MediaData data = new MediaData(0, cursor.getLong(cursor
                            .getColumnIndex(AudioMedia._ID)),
                            MediaData.NOTPLAYED, cursor.getLong(cursor
                                    .getColumnIndex(AudioMedia.DURATION)),
                            cursor.getString(cursor
                                    .getColumnIndex(AudioMedia.TITLE)),
                            cursor.getString(cursor
                                    .getColumnIndex(AudioMedia.ALBUM)),
                            cursor.getString(cursor
                                    .getColumnIndex(AudioMedia.ARTIST)),
                            cursor.getString(cursor
                                    .getColumnIndex(AudioMedia.DATA)));
                    return data;
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        return null;
    }

    @Override
    protected void messageHandle(int what, List<Integer> items) {
        if (items.size() > 0) {
            switch (what) {
            case SystemConsts.EVT_SELECT_ADD: {
                ArrayList<Long> idlist = new ArrayList<Long>();
                for (int i = 0; i < items.size(); i++) {
                    int index = items.get(i);
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(index);
                    if (selectedCursor != null) {
                        long mediaId = selectedCursor.getLong(selectedCursor
                                .getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID));
                        if (mediaId >= 0) {
                            idlist.add(mediaId);
                        }
                    }
                }
                long[] ids = new long[idlist.size()];
                for (int i = 0; i < idlist.size(); i++) {
                    ids[i] = idlist.get(i);
                }

                AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(
                        getActivity(), getFragmentManager(), ids);
                task.setHideOrderMenu(true);
                task.execute();
            }
                break;
            case SystemConsts.EVT_SELECT_RATING: {
                ArrayList<Long> ids = new ArrayList<Long>();
                for (int i = 0; i < items.size(); i++) {
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(items
                            .get(i));
                    if (selectedCursor != null) {
                        long mediaId = selectedCursor.getLong(selectedCursor
                                .getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID));
                        if (mediaId >= 0) {
                            ids.add(mediaId);
                        }
                    }
                }

                RatingDialog dlg = new RatingDialog(getActivity(),
                        TableConsts.FAVORITE_TYPE_SONG,
                        ids.toArray(new Long[ids.size()]), mHandler);
                dlg.show();
            }
                break;
            case SystemConsts.EVT_SELECT_DEL: {
                final Integer[] removeItems = items.toArray(new Integer[items
                        .size()]);
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        getActivity());
                builder.setTitle(R.string.lb_confirm);
                builder.setMessage(String.format(
                        getString(R.string.fmt_remove_orderlist), items.size()));
                builder.setPositiveButton(R.string.lb_ok,
                        new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                try {
                                    for (Integer i : removeItems) {
                                        getBinder().setContentsKey(null);
                                        getBinder().remove(i);
                                    }
                                } catch (RemoteException e) {
                                }
                            }
                        });
                builder.setNegativeButton(R.string.lb_cancel, null);
                builder.show();
            }
                break;
            case SystemConsts.EVT_SELECT_CLEARCACHE: {
                for (Integer i : items) {
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(i);
                    if (selectedCursor != null) {
                        long mediaId = selectedCursor.getLong(selectedCursor
                                .getColumnIndex(AudioMedia._ID));
                        String data = selectedCursor.getString(selectedCursor
                                .getColumnIndex(MediaConsts.AudioMedia.DATA));

                        ContentsUtils.clearMediaCache(getActivity(), data);
                        ContentValues values = new ContentValues();
                        values.put(TableConsts.AUDIO_CACHE_FILE, (String) null);
                        getActivity()
                                .getContentResolver()
                                .update(ContentUris.withAppendedId(
                                        MediaConsts.MEDIA_CONTENT_URI, mediaId),
                                        values, null, null);
                    }
                }
                datasetChanged();
            }
                break;
            case SystemConsts.EVT_SELECT_LOVE:
            case SystemConsts.EVT_SELECT_BAN: {
                for (Integer i : items) {
                    messageHandle(what, i);
                }
            }
                break;
            case SystemConsts.EVT_SELECT_CHECKALL: {
                for (int i = 0; i < mListView.getCount(); i++) {
                    mListView.setItemChecked(i, true);
                }
            }
                break;
            default: {
                messageHandle(what, items.get(0));
            }
            }
        }
    }

    protected void messageHandle(int what, final int selectedPosition) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(selectedPosition);
        if (selectedCursor != null) {
            long mediaId = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID));

            switch (what) {
            case SystemConsts.EVT_SELECT_ARTIST: {
                MediaData data = loadMediaItem(mediaId);
                if (data != null) {
                    String artist = data.getArtist();

                    getFragmentManager().popBackStack(
                            SystemConsts.TAG_SUBFRAGMENT,
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    FragmentTransaction t = getFragmentManager()
                            .beginTransaction();
                    AnimationHelper.setFragmentToPlayBack(mPref, t);
                    t.add(getId(),
                            jp.co.kayo.android.localplayer.fragment.ArtistListFragment
                                    .createFragment(null, artist, -1,
                                            FragmentUtils.cloneBundle(this)));
                    t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                    t.hide(this);
                    t.commit();
                }
            }
                break;
            case SystemConsts.EVT_SELECT_OPENALBUM: {
                Hashtable<String, String> tbl = ContentsUtils.getMedia(
                        getActivity(), new String[] { AudioMedia.ALBUM_KEY },
                        mediaId);
                String album_key = tbl.get(AudioMedia.ALBUM_KEY);
                if (album_key != null) {
                    getFragmentManager().popBackStack(
                            SystemConsts.TAG_SUBFRAGMENT,
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    FragmentTransaction t = getFragmentManager()
                            .beginTransaction();
                    AnimationHelper.setFragmentToPlayBack(mPref, t);
                    t.add(getId(),
                            AlbumSongsFragment.createFragment(album_key,
                                    FragmentUtils.cloneBundle(this)));
                    t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                    t.hide(this);
                    t.commit();
                }
            }
                break;
            case SystemConsts.EVT_SELECT_MORE: {
                MediaData data = loadMediaItem(mediaId);
                if (data != null) {
                    String album = data.getAlbum();
                    String artist = data.getArtist();
                    String title = data.getTitle();
                    showInfoDialog(album, artist, title);
                }
            }
                break;
            case SystemConsts.EVT_SELECT_LOVE: {
                MediaData data = loadMediaItem(mediaId);
                if (data != null) {
                    String album = data.getAlbum();
                    String artist = data.getArtist();
                    String title = data.getTitle();
                    long duration = data.getDuration();
                    ContentsUtils.lastfmLove(getActivity(), title, artist,
                            album, Long.toString(duration));
                }
            }
                break;
            case SystemConsts.EVT_SELECT_BAN: {
                MediaData data = loadMediaItem(mediaId);
                if (data != null) {
                    String album = data.getAlbum();
                    String artist = data.getArtist();
                    String title = data.getTitle();
                    long duration = data.getDuration();
                    ContentsUtils.lastfmBan(getActivity(), title, artist,
                            album, Long.toString(duration));
                }
            }
                break;
            case SystemConsts.EVT_SELECT_EDIT: {
                // 文字化け修正
                Intent intent = new Intent(getActivity(), TagEditActivity.class);
                intent.putExtra(SystemConsts.KEY_EDITTYPE,
                        TagEditActivity.MEDIA);
                intent.putExtra(SystemConsts.KEY_EDITKEY,
                        Long.toString(mediaId));
                getActivity().startActivityForResult(intent,
                        SystemConsts.REQUEST_TAGEDIT);
            }
                break;
            default: {
            }
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, final int position, long id) {
        if (mActionMode.hasMenu()) {
            mActionMode.onItemClick(l, v, position, id);
        } else {
            final Cursor cursor = (Cursor) getListAdapter().getItem(position);
            final BaseActivity base = (BaseActivity) getActivity();
            final IMediaPlayerService binder = base.getBinder();
            final long media_id = cursor.getLong(cursor
                    .getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID));
            if (binder != null) {
                AsyncTask<Void, Void, Void> animTask = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            binder.reset();
                            binder.setPosition(position);
                            binder.play();

                            mViewcache.setCurrentPos(position);
                            mViewcache.setCurrentId(media_id);
                        } catch (RemoteException e) {
                        }
                        return null;
                    }
                };
                animTask.execute();
            }
        }
    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }

    @Override
    public int getFragmentId() {
        return R.layout.playorder_grid_view;
    }

    String beforeSetting = null;

    public void setAlbumImage() {
        if (imageAlbumArt == null) {
            return;
        }
        final View myView = getView();
        final Context myContext = getActivity();
        if (myView == null || myContext == null) {
            return;
        }
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                String currentAlbumKey = null;
                long mediaId = mViewcache.getCurrentId();
                if (mediaId >= 0) {
                    View view = myView.findViewById(R.id.playViewLayout);
                    myHandler.sendMessage(myHandler.obtainMessage(
                            EVT_SET_VISIBLE, view));

                    Hashtable<String, String> tbl = ContentsUtils.getMedia(
                            myContext, new String[] { AudioMedia.ALBUM_KEY },
                            mediaId);
                    if (tbl != null && tbl.size() > 0) {
                        currentAlbumKey = tbl.get(AudioMedia.ALBUM_KEY);
                        Hashtable<String, String> tbl1 = new Hashtable<String, String>();
                        Hashtable<String, String> tbl2 = new Hashtable<String, String>();
                        Bitmap bitmap = null;
                        // アルバム情報を取得
                        bitmap = Funcs.getAlbumArt(myContext, mediaId, -1,
                                tbl1, tbl2);
                        if (bitmap != null) {
                            myHandler.sendMessage(myHandler.obtainMessage(
                                    EVT_SET_IMAGE, bitmap));
                        } else if (tbl2.size() > 0) {
                            myHandler.sendMessage(myHandler.obtainMessage(
                                    EVT_SET_IMAGE, null));
                            mViewcache.getUnManagerImage(myContext,
                                    tbl2.get(AudioAlbum.ALBUM),
                                    tbl2.get(AudioAlbum.ARTIST),
                                    currentAlbumKey, new MyImageObserver(),
                                    imagesize);
                        }
                        // レーティングを設定
                        FavoriteInfo inf = mViewcache.getFavorite(myContext,
                                mediaId, TableConsts.FAVORITE_TYPE_SONG);
                        myHandler.sendMessage(myHandler.obtainMessage(
                                EVT_SET_TEXT1, tbl1.get(AudioAlbum.TITLE)));
                        myHandler.sendMessage(myHandler.obtainMessage(
                                EVT_SET_TEXT2, tbl1.get(AudioAlbum.ALBUM)));
                        myHandler.sendMessage(myHandler.obtainMessage(
                                EVT_SET_TEXT3, tbl1.get(AudioAlbum.ARTIST)));
                        myHandler.sendMessage(myHandler.obtainMessage(
                                EVT_SET_RATING, inf));
                    }
                } else {
                    View view = myView.findViewById(R.id.playViewLayout);
                    myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_GONE,
                            view));
                }
                return null;
            }

        };
        task.execute();
    }

    private void setBitmap(Bitmap bm) {
        if (imageAlbumArt == null) {
            if (bm != null) {
                bm.recycle();
                bm = null;
            }
            return;
        }
        if (sourcebmp != null) {
            sourcebmp.recycle();
            sourcebmp = null;
        }
        sourcebmp = bm;
        imageAlbumArt.setImageBitmap(bm);
        imageAlbumArt.requestLayout();
    }

    class MyImageObserver implements ImageObserver {

        @Override
        public void onLoadImage(final Bitmap bmp) {
            myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_IMAGE, bmp));
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Logger.d("onCreateLoader:" + mAdapter);
        Uri orderuri = Uri
                .parse(DeviceContentProvider.MEDIA_CONTENT_AUTHORITY_SLASH
                        + "order/audio");
        if (mAdapter == null) {
            mAdapter = new PlaybackListViewAdapter(getActivity(), null,
                    mViewcache);
            getListView().setOnTouchListener(
                    new FastOnTouchListener(new Handler(), mAdapter));
            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        showProgressBar();
        String cur_contenturi = mPref.getContentUri();
        return new CursorLoader(getActivity(), orderuri, null,
                TableConsts.PLAYBACK_URI + " = ?",
                new String[] { cur_contenturi }, TableConsts.PLAYBACK_ORDER);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Logger.d("onLoadFinished:" + mAdapter);
        try {
            if (data != null && !data.isClosed()) {
                if (mAdapter == null) {
                    mAdapter = new PlaybackListViewAdapter(getActivity(), data,
                            mViewcache);
                    getListView().setOnTouchListener(
                            new FastOnTouchListener(new Handler(), mAdapter));
                    setListAdapter(mAdapter);
                } else if (mAdapter != null) {
                    if (data == null || data.isClosed()) {
                        if (data == null)
                            mAdapter.swapCursor(null);
                        return;
                    }
                    Cursor cur = mAdapter.swapCursor(data);
                }
            }
        } finally {
            hideProgressBar();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
    }

    @Override
    public void startProgress(final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mViewcache.startProgress(max);
                    IMediaPlayerService binder = getBinder();
                    if (binder != null) {
                        try {
                            mViewcache.setPrefetchId(binder.getPrefetchId());
                        } catch (RemoteException e) {
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void stopProgress() {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mViewcache.stopProgress();
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void progress(final long pos, final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mViewcache.progress(pos, max);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = mPref.getHideTime();
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mActionMode != null && mActionMode.cancelActionMode()) {
            return true;
        }
        return false;
    }

    @Override
    public String selectSort() {
        return null;
    }

    @Override
    public void hideMenu() {
        if (mActionMode != null) {
            mActionMode.cancelActionMode();
        }
    }

    @Override
    protected void datasetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void addRow(Object[] values) {
        // TODO Auto-generated method stub

    }

    private Timer mTimer = null;
    private boolean mAlbumartScale = false;
    private int mOrgLayoutSize;
    private int mScaleLayoutSize;

    @Override
    public void onClick(View v) {
        Configuration config = getResources().getConfiguration();
        if (config.orientation == Configuration.ORIENTATION_PORTRAIT
                || getFragmentManager().findFragmentByTag(
                        SystemConsts.TAG_EQUALIZER) != null) {
            int id = v.getId();
            switch (id) {
            case R.id.imageAlbumArt:
                if (mTimer == null) {
                    if (mAlbumartScale == false) {
                        // アルバムアートの拡大
                        scaleUpAlbumArt();
                        mAlbumartScale = true;
                    } else {
                        // アルバムアートを元のサイズに戻す
                        scaleDownAlbumArt();
                        mAlbumartScale = false;
                    }
                }
                break;
            case R.id.playViewLayout:
                if (mTimer == null) {
                    if (mAlbumartScale == true) {
                        // アルバムアートを元のサイズに戻す
                        scaleDownAlbumArt();
                        mAlbumartScale = false;
                    }
                }
                break;
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (v.getId() == R.id.imageAlbumArt) {
            long mediaId = mViewcache.getCurrentId();
            if (mediaId >= 0) {
                Intent i = new Intent(getActivity(), AlbumartActivity.class);
                i.putExtra("media_key", mediaId);
                getActivity().startActivityForResult(i,
                        SystemConsts.REQUEST_ALBUMART);
                return true;
            }
        }
        return false;
    }

    /**
     * アルバムアートを拡大
     */
    private void scaleUpAlbumArt() {
        // 背景のCDジャケットを消す
        imageAlbumArt.setBackgroundColor(Color.argb(00, 00, 00, 00));

        // 拡大サイズを設定
        mOrgLayoutSize = mPlayViewLayout.getHeight();
        mScaleLayoutSize = getView().findViewById(R.id.mainPlayWrapperFrame)
                .getHeight();
        long time = mPref.getHideTime();
        if (time != 0) {
            View controller = getActivity().findViewById(
                    R.id.controlLinearLayout01);
            mScaleLayoutSize -= controller.getHeight();
        }

        // 拡大アニメーションを設定
        float toX = Float.valueOf(getString(R.string.scaleX));
        float toY = Float.valueOf(getString(R.string.scaleY));
        int pivotX = getResources().getInteger(R.integer.pivotX);
        int pivotY = getResources().getInteger(R.integer.pivotY);

        // 拡大アニメーションを開始
        ScaleAnimation scale = new ScaleAnimation(1, toX, 1, toY, pivotX,
                pivotY);
        scale.setDuration(300);
        scale.setFillAfter(true);
        imageAlbumArt.startAnimation(scale);

        // アルバムアートの拡大に合わせてレイアウトの高さも変更
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, mScaleLayoutSize);
        mPlayViewLayout.setOrientation(LinearLayout.VERTICAL);
        mPlayViewLayout.setLayoutParams(params);

        // 曲情報をアルバムアートの下に移動
        TypedValue typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.albumBackground,
                typedValue, true);

        mSongInfoLayout.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
        float bottomDp = Funcs.getDimens(getActivity(), R.dimen.padding_xlarge);
        mSongInfoLayout.setPadding(0, 0, 0, (int) bottomDp);

        // mTextHeight = txtTitle1.getHeight();
        txtTitle1.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        txtTitle2.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        txtTitle3.setVisibility(View.GONE);
        ratingBar1.setVisibility(View.VISIBLE);

        // テキスト情報を2行表示に変更
        mOrgTitle = txtTitle1.getText().toString();
        mOrgArtist = txtTitle2.getText().toString();
        mOrgAlbum = txtTitle3.getText().toString();
        txtTitle1.setText(mOrgTitle + " - " + mOrgAlbum);
        txtTitle2.setText(mOrgArtist);

        // font size
        txtTitle1.setTextSize(getResources().getDimension(
                R.dimen.scaleUpFontSize2));
        txtTitle2.setTextSize(getResources().getDimension(
                R.dimen.scaleUpFontSize2));
        txtTitle3.setTextSize(getResources().getDimension(
                R.dimen.scaleUpFontSize2));

        int height = Funcs.getDimens(getActivity(),
                R.dimen.fragment_playbacklistview_text_title_height_expand);
        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, height);
        txtTitle1.setLayoutParams(textParams);
        txtTitle2.setLayoutParams(textParams);
        txtTitle3.setLayoutParams(textParams);

        // ソングリストを隠す
        ListView listView = getListView();
        if (listView != null) {
            listView.setVisibility(View.INVISIBLE);
        }

        // 再生コントロールを表示状態にする
        ControlFragment control = (ControlFragment) getFragmentManager()
                .findFragmentByTag("tag_control");
        control.showControl(true);
    }

    /**
     * アルバムアートを縮小
     */
    private void scaleDownAlbumArt() {
        // 拡大サイズを設定
        mScaleLayoutSize = mOrgLayoutSize;

        // 縮小アニメーション開始
        float fromX = Float.valueOf(getResources().getString(R.string.scaleX));
        float fromY = Float.valueOf(getResources().getString(R.string.scaleY));
        int pivotX = getResources().getInteger(R.integer.pivotX);
        int pivotY = getResources().getInteger(R.integer.pivotY);
        ScaleAnimation scale = new ScaleAnimation(fromX, 1, fromY, 1, pivotX,
                pivotY);
        scale.setDuration(300);
        scale.setFillAfter(true);
        imageAlbumArt.startAnimation(scale);

        // アルバムアートの拡大に合わせてレイアウトの高さを変更
        if (mTimer == null) {
            mTimer = new Timer(true);
            final Handler handler = new Handler();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        public void run() {
                            // 背景のCDジャケットをセット
                            imageAlbumArt
                                    .setBackgroundResource(R.drawable.album_border_mini);

                            // アルバムアート表示領域を元に戻す
                            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    mOrgLayoutSize);
                            mPlayViewLayout
                                    .setOrientation(LinearLayout.HORIZONTAL);
                            mPlayViewLayout.setLayoutParams(params);

                            // font size
                            txtTitle1.setTextSize(getResources().getDimension(
                                    R.dimen.scaleDownFontSize2));
                            txtTitle2.setTextSize(getResources().getDimension(
                                    R.dimen.scaleDownFontSize2));
                            txtTitle3.setTextSize(getResources().getDimension(
                                    R.dimen.scaleDownFontSize2));

                            // 曲情報を元の位置に戻す
                            txtTitle1.setText(mOrgTitle);
                            txtTitle2.setText(mOrgArtist);
                            txtTitle3.setText(mOrgAlbum);
                            int height = Funcs
                                    .getDimens(
                                            getActivity(),
                                            R.dimen.fragment_playbacklistview_text_title_height_normal);
                            LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT, height);

                            txtTitle1.setLayoutParams(textParams);
                            txtTitle2.setLayoutParams(textParams);
                            txtTitle3.setLayoutParams(textParams);
                            txtTitle1.setGravity(Gravity.BOTTOM);
                            txtTitle2.setGravity(Gravity.BOTTOM);
                            txtTitle3.setVisibility(View.VISIBLE);
                            ratingBar1.setVisibility(View.INVISIBLE);
                            mSongInfoLayout.setGravity(Gravity.CENTER);
                            int padding = Funcs.getDimens(getActivity(),
                                    R.dimen.padding_normal);
                            mSongInfoLayout.setPadding(padding, padding,
                                    padding, padding);

                            // ソングリストを表示
                            ListView listView = getListView();
                            if (listView != null) {
                                listView.setVisibility(View.VISIBLE);
                            }

                            if (mTimer != null) {
                                mTimer.cancel();
                                mTimer = null;
                            }
                        }
                    });
                }
            }, 200, 1000);
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_order_name);
    }

    @Override
    public void doSearchQuery(String queryString) {
        // TODO Auto-generated method stub

    }

    static final int EVT_SET_TEXT1 = 100;
    static final int EVT_SET_TEXT2 = 101;
    static final int EVT_SET_TEXT3 = 102;
    static final int EVT_SET_RATING = 103;
    static final int EVT_SET_IMAGE = 104;
    static final int EVT_SET_VISIBLE = 105;
    static final int EVT_SET_GONE = 106;

    static class UpdateMyHandler extends Handler {
        WeakReference<PlaybackListViewFragment> ref;

        UpdateMyHandler(PlaybackListViewFragment f) {
            ref = new WeakReference<PlaybackListViewFragment>(f);
        }

        @Override
        public void handleMessage(Message msg) {
            PlaybackListViewFragment f = ref.get();
            if (f != null) {
                switch (msg.what) {
                case EVT_SET_VISIBLE: {
                    View view = (View) msg.obj;
                    view.setVisibility(View.VISIBLE);
                }
                    break;
                case EVT_SET_GONE: {
                    View view = (View) msg.obj;
                    view.setVisibility(View.GONE);
                }
                    break;
                case EVT_SET_TEXT1: {
                    f.mOrgTitle = (String) msg.obj;
                    f.txtTitle1.setText(f.mOrgTitle);

                }
                    break;
                case EVT_SET_TEXT2: {
                    f.mOrgArtist = (String) msg.obj;
                    f.txtTitle2.setText(f.mOrgArtist);
                }
                    break;
                case EVT_SET_TEXT3: {
                    f.mOrgAlbum = (String) msg.obj;
                    f.txtTitle3.setText(f.mOrgAlbum);
                }
                    break;
                case EVT_SET_RATING: {
                    FavoriteInfo inf = (FavoriteInfo) msg.obj;
                    f.ratingBar1.setRating(inf.rating);
                }
                    break;
                case EVT_SET_IMAGE: {
                    f.setBitmap((Bitmap) msg.obj);
                }
                    break;
                }
            }
        }
    }
}
