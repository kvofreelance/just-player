package jp.co.kayo.android.localplayer.adapter;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.core.ProgressCursorAdapter;
import jp.co.kayo.android.localplayer.core.ViewHolder;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.FavoriteInfo;
import jp.co.kayo.android.localplayer.util.bean.PlaylistMemberInfo;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

public class PlaylistSongsAdapter extends ArrayAdapter<PlaylistMemberInfo> {
    Context context;
    ViewCache viewCache;
    LayoutInflater inflator;
    Handler handler = new Handler();

    public PlaylistSongsAdapter(Context context, List<PlaylistMemberInfo> list, ViewCache cache) {
        super(context, R.layout.playlist_edit_row, list);
        this.context = context;
        this.viewCache = cache;
    }

    public LayoutInflater getInflator() {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            convertView = getInflator().inflate(R.layout.playlist_edit_row, parent, false);
            holder = new ViewHolder();
            holder.setText1((TextView) convertView.findViewById(R.id.text1));
            viewCache.getColorset().setColor(ColorSet.KEY_DEFAULT_PRI_COLOR, holder.getText1());
            holder.setText2((TextView) convertView.findViewById(R.id.text2));
            viewCache.getColorset().setColor(ColorSet.KEY_DEFAULT_PRI_COLOR, holder.getText2());
            holder.setText3((TextView) convertView.findViewById(R.id.text3));
            viewCache.getColorset().setColor(ColorSet.KEY_DEFAULT_SEC_COLOR, holder.getText3());
            holder.setText4((TextView) convertView.findViewById(R.id.text4));
            viewCache.getColorset().setColor(ColorSet.KEY_DEFAULT_SEC_COLOR, holder.getText4());
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        
        PlaylistMemberInfo item = getItem(position);

        String title = item.title;
        String artist = item.artist;
        int track = item.track;
        long duration = item.duration;
        
        
        holder.getText1().setText(Funcs.getTrack(track));
        holder.getText2().setText(title);
        holder.getText3().setText(artist);
        holder.getText4().setText(Funcs.makeTimeString(duration, false));

        return convertView;
    }
}
