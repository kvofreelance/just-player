package jp.co.kayo.android.localplayer.adapter;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.core.ImageObserverImpl;
import jp.co.kayo.android.localplayer.core.IndexCursorAdapter;
import jp.co.kayo.android.localplayer.core.ViewHolder;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.FavoriteInfo;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class AlbumGridViewAdapter extends IndexCursorAdapter {
    LayoutInflater inflator;
    Handler handler = new Handler();

    int getColAlbum(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioAlbum.ALBUM);
    }

    int getColArtist(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioAlbum.ARTIST);
    }

    int getColArt(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioAlbum.ALBUM_ART);
    }

    int getColRating(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioAlbum.FAVORITE_POINT);
    }

    public AlbumGridViewAdapter(Context context, Cursor c, ViewCache cache,
            String cname) {
        super(context, c, true, cache, cname);
    }

    public LayoutInflater getInflator(Context context) {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        long id = cursor.getLong(cursor
                .getColumnIndex(MediaConsts.AudioAlbum._ID));
        String album = cursor.getString(getColAlbum(cursor));
        String artist = cursor.getString(getColArtist(cursor));
        String album_art = cursor.getString(getColArt(cursor));
        int col = getColRating(cursor);

        holder.setPotision(cursor.getPosition());
        if (MediaConsts.AudioAlbum.ALBUM.equals(getCname())) {
            holder.getText2().setText(album);
            holder.getText3().setText(artist);
        } else {
            holder.getText2().setText(artist);
            holder.getText3().setText(album);
        }
        if (col != -1) {
            holder.getRating1().setRating(cursor.getInt(col));
        } else {
            FavoriteInfo inf = getViewcache().getFavorite(context, id,
                    TableConsts.FAVORITE_TYPE_ALBUM);
            holder.getRating1().setRating(inf.rating);
        }

        if (!imageloadskip) {
            Integer key = Funcs.getAlbumKey(album, artist);
            Bitmap bmp = getViewcache().getImage(album, artist, album_art,
                    new ImageObserverImpl(handler, getViewcache(), holder, key,
                            cursor.getPosition()));
            if (bmp != null) {
                holder.getImage1().setImageBitmap(bmp);
            } else {
                holder.getImage1().setImageBitmap(null);
            }
        } else {
            holder.getImage1().setImageBitmap(null);
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = getInflator(context).inflate(R.layout.img_grid_row1, parent,
                false);
        ViewHolder holder = new ViewHolder();
        holder.setImage1((ImageView) v.findViewById(R.id.imageArt));
        holder.setText2((TextView) v.findViewById(R.id.text2));
        getViewcache().getColorset().setColor(ColorSet.KEY_DEFAULT_PRI_COLOR, holder.getText2());
        holder.setText3((TextView) v.findViewById(R.id.text3));
        getViewcache().getColorset().setColor(ColorSet.KEY_DEFAULT_SEC_COLOR, holder.getText3());
        holder.setRating1((RatingBar) v.findViewById(R.id.ratingBar1));
        v.setTag(holder);
        return v;
    }
}
