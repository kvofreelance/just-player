package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.adapter.PlaylistListAdapter;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.task.ProgressTask;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfo;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfoLoader;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v4.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

@SuppressLint("NewApi")
public class AddPlaylistDialog extends DialogFragment implements LoaderCallbacks<List<PlaylistInfo>>, OnItemClickListener {
    private MyPreferenceManager mPref;
    private ViewCache mViewcache;
    private PlaylistListAdapter mAdapter;
    long[] mPlaylist;
    private ListView mListView;
    private boolean hideOrder = false;
    private boolean addNew = true;
    private DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    
    ViewCache getCache() {
        if (mViewcache == null) {
            mViewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                    SystemConsts.TAG_CACHE);
        }
        return mViewcache;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.playlist_grid_view, null, false);
        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setOnItemClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.lb_newnewplaylist));
        builder.setNegativeButton(getString(R.string.lb_cancel), null);
        builder.setView(view);
        builder.setInverseBackgroundForced(new ThemeHelper().isInverseBackgroundForced(getActivity()));

        return builder.create();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = new MyPreferenceManager(getActivity());
        Bundle args = getArguments();
        if (args != null) {
            mPlaylist = args.getLongArray("playlist");
            hideOrder = args.getBoolean("hideOrderMenu");
            addNew = args.getBoolean("addNewMenu");
        }
        
        setStyle(DialogFragment.STYLE_NORMAL, new ThemeHelper().getTheme(getActivity()));
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        
        getLoaderManager().initLoader(R.layout.playlist_grid_view, null, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(R.layout.playlist_grid_view);
    }

    private void addPlayback(Context context, IMediaPlayerService binder, ProgressFragment dialog){
        if(binder!=null){
            try{
                dialog.setMax(mPlaylist.length);
                int pos = binder.getCount();
                boolean addMedia = false;
                for (int i = 0; i < mPlaylist.length; i++) {
                    dialog.setProgress(i);
                    long id = mPlaylist[i];
                    Hashtable<String, String> tbl = ContentsUtils.getMedia(context, new String[]{AudioMedia.DATA}, id);
                    if(tbl!=null && tbl.size()>0){
                        addMedia = true;
                        binder.addMedia(id, tbl.get(AudioMedia.DATA));
                    }
                }
                binder.commit();
                if(addMedia){
                    int stat = binder.stat();
                    if ((stat & AppWidgetHelper.FLG_PLAY) == 0) {
                        if(pos<0){
                            pos = 0;
                        }
                        binder.setPosition(pos);
                        binder.reset();
                        binder.play();
                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }finally{
                
            }
        }
    }

    private void addPlaylist(Context context, IMediaPlayerService binder, ProgressFragment dialog, long playlist_id) {
        Uri playlisturi = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, playlist_id);
        int order = 0;
        // Count
        Cursor cursor = null;
        try{
            cursor = context.getContentResolver().query(
                    ContentUris.withAppendedId(
                    MediaConsts.PLAYLIST_CONTENT_URI,
                    playlist_id), new String[]{AudioPlaylistMember._ID}, null, null, null);
            if(cursor!=null){
                order = cursor.getCount();
            }
        }
        finally{
            if(cursor!=null){
                cursor.close();
            }
        }
        // AllInsert
        dialog.setMax(mPlaylist.length);
        for (int i = 0; i < mPlaylist.length; i++) {
            dialog.setProgress(i);
            long id = mPlaylist[i];
            ContentValues values = new ContentValues();
            values.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, id);
            values.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID,
                    playlist_id);
            values.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, order+i);
            context.getContentResolver().insert(playlisturi, values);
        }
        //Toast.makeText(getActivity(), getString(R.string.txt_action_addsong), Toast.LENGTH_SHORT).show();
    }
    
    private void savePlaylist(Context context, String name, ProgressFragment dialog) {
        ContentValues newplaylist = new ContentValues();
        long playlist_key = System.nanoTime();//外部でプレイリストを管理するシステムと整合をあわせるためダミー用のIDを作っておく
        newplaylist.put(AudioPlaylist.PLAYLIST_KEY, Long.toString(playlist_key));
        newplaylist.put(MediaStore.Audio.Playlists.NAME, name);
        // AllInsert
        Uri uri = getActivity().getContentResolver().insert(
                MediaConsts.PLAYLIST_CONTENT_URI, newplaylist);
        dialog.setMax(mPlaylist.length);
        for (int i = 0; i < mPlaylist.length; i++) {
            dialog.setProgress(i);
            long id = mPlaylist[i];
            ContentValues values = new ContentValues();
            values.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, id);
            values.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID,
                    ContentUris.parseId(uri));
            values.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, i);
            context.getContentResolver().insert(uri, values);
        }
        
        context.getContentResolver().notifyChange(MediaConsts.ALBUM_CONTENT_URI,
                null);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
            long arg3) {
        PlaylistInfo item = mAdapter.getItem(position);
        if(item.id == PlaylistInfoLoader.PLAYLIST_ORDER_ID){
            //再生リストに追加
            final Context context = getActivity();
            final IMediaPlayerService binder = getBinder();
            ProgressTask progTask = new ProgressTask(getFragmentManager()) {
                @Override
                protected Void doInBackground(Void... params) {
                    addPlayback(context, binder, this.dialog);
                    return null;
                }
            };
            progTask.execute();
        }
        if(item.id == PlaylistInfoLoader.PLAYLIST_ADDNEW_ID){
            //新規プレイリストに追加
            final EditText editText = new EditText(getActivity());
            final Context context = getActivity();
            StringBuilder buf = new StringBuilder();
            Calendar cal = Calendar.getInstance();
            String date = dfm.format(cal.getTime());
            buf.append(getString(R.string.lb_newplaylist)).append(date);
            editText.setText(buf.toString());
            
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getString(R.string.lb_save_playback));
            builder.setView(editText);
            builder.setPositiveButton(getString(R.string.lb_ok),
                    new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final String fname = editText.getText().toString();
                            ProgressTask progTask = new ProgressTask(getFragmentManager()) {
                                @Override
                                protected Void doInBackground(Void... params) {
                                    savePlaylist(context, fname, this.dialog);
                                    return null;
                                }
                            };
                            progTask.execute();
                            AddPlaylistDialog.this.dismiss();
                        }
                    });
            builder.setNegativeButton(getString(R.string.lb_cancel), null);
            builder.setInverseBackgroundForced(new ThemeHelper().isInverseBackgroundForced(getActivity()));
            builder.show();
            return;
        }
        else{
            final Context context = getActivity();
            final IMediaPlayerService binder = getBinder();
            final long id = item.id;
            ProgressTask progTask = new ProgressTask(getFragmentManager()) {
                @Override
                protected Void doInBackground(Void... params) {
                    addPlaylist(context, binder, this.dialog, id);
                    return null;
                }
            };
            progTask.execute();
        }
        AddPlaylistDialog.this.dismiss();
    }

    public Loader<List<PlaylistInfo>> onCreateLoader(int arg0, Bundle arg1) {

        PlaylistInfoLoader loader = new PlaylistInfoLoader(getActivity(), !hideOrder, addNew);
        loader.forceLoad();
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<PlaylistInfo>> arg0, List<PlaylistInfo> arg1) {
        mAdapter = new PlaylistListAdapter(getActivity(), arg1, getCache());
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<List<PlaylistInfo>> arg0) {
        mAdapter = new PlaylistListAdapter(getActivity(), new ArrayList<PlaylistInfo>(),
                getCache());
        mListView.setAdapter(mAdapter);
    }

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }
}
