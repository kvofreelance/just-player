package jp.co.kayo.android.localplayer;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ImageObserverImpl;
import jp.co.kayo.android.localplayer.core.ViewHolder;
import jp.co.kayo.android.localplayer.dialog.EnocdeStringListDialog;
import jp.co.kayo.android.localplayer.dialog.EnocdeStringListDialog.Callback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerServiceCallback;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.StrictHelper;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import jp.co.kayo.android.localplayer.util.ViewCache;

public class TagEditActivity extends BaseActivity implements OnClickListener,
        Callback {
    public static final int ALBUM = 0;
    public static final int ARTIST = 1;
    public static final int MEDIA = 2;
    private int editType;
    private String editKey;
    private String garbledText;
    private String albumKey;
    private String artistKey;
    private String mediaKey;
    private long targetId;
    ViewHolder mHolder;
    
    private EditText editArtist;
    private EditText editAlbum;
    private EditText editTitle;

    private ViewCache mViewCache;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        StrictHelper.registStrictMode();
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        super.onCreate(savedInstanceState);

        hideProgressBar();

        setContentView(R.layout.tagediter_album);

        editArtist = (EditText) findViewById(R.id.editArtist);
        editAlbum = (EditText) findViewById(R.id.editAlbum);
        editTitle = (EditText) findViewById(R.id.editTitle);
        mHolder = new ViewHolder();
        mHolder.setImage1((ImageView) findViewById(R.id.imageAlbumArt));
        mHolder.setText1((TextView) findViewById(R.id.txtTitle1));
        mHolder.setText2((TextView) findViewById(R.id.txtTitle2));
        mHolder.setText3((TextView) findViewById(R.id.txtTitle3));

        findViewById(R.id.imageAlbumArt).setOnClickListener(this);
        findViewById(R.id.btnGarbled).setOnClickListener(this);
        findViewById(R.id.btnApply).setOnClickListener(this);
        findViewById(R.id.btnBack).setOnClickListener(this);

        // ViewCache
        mViewCache = (ViewCache) getSupportFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);

        Intent intent = getIntent();
        if (intent != null) {
            editType = intent.getIntExtra(SystemConsts.KEY_EDITTYPE, -1);
            editKey = intent.getStringExtra(SystemConsts.KEY_EDITKEY);
        }

        if (editType == ALBUM) {
            setupAlbumEdit();
        } else if (editType == ARTIST) {
            setupArtistEdit();
        } else if (editType == MEDIA) {
            setupMediaEdit();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setupArtistEdit() {
        findViewById(R.id.textAlbum).setVisibility(View.GONE);
        editAlbum.setVisibility(View.GONE);
        findViewById(R.id.textTitle).setVisibility(View.GONE);
        editTitle.setVisibility(View.GONE);

        Hashtable<String, String> tbl = ContentsUtils.getArtist(this,
                new String[] { AudioArtist._ID, AudioArtist.ARTIST_KEY,
                        AudioArtist.ARTIST }, editKey);
        if (tbl != null) {
            targetId = Funcs.parseLong(tbl.get(AudioArtist._ID));
            artistKey = tbl.get(AudioArtist.ARTIST_KEY);
            String artist = tbl.get(AudioArtist.ARTIST);

            garbledText = artist;
            mHolder.getText1().setText(artist);
            mHolder.getText2().setText("");
            editArtist.setText(artist);
            Integer key = Funcs.getAlbumKey(null, artistKey);
            Bitmap bmp = mViewCache
                    .getImage(null, artistKey, null, new ImageObserverImpl(
                            mHandler, mViewCache, mHolder, key, 0));
            if (bmp != null) {
                mHolder.getImage1().setImageBitmap(bmp);
            } else {
                mHolder.getImage1().setImageBitmap(null);
            }
        }

    }

    private void setupMediaEdit() {
                
        Hashtable<String, String> tbl = ContentsUtils.getMedia(this,
                new String[] { AudioMedia._ID, AudioMedia.ALBUM,
                        AudioMedia.ARTIST, AudioMedia.TITLE,
                        AudioMedia.ALBUM_KEY }, Long.parseLong(editKey));

        if (tbl != null) {
            mediaKey = editKey;
            targetId = Funcs.parseLong(tbl.get(AudioAlbum._ID));
            String album = tbl.get(AudioMedia.ALBUM);
            String artist = tbl.get(AudioMedia.ARTIST);
            String title = tbl.get(AudioMedia.TITLE);
            //albumKey = tbl.get(AudioMedia.ALBUM_KEY);
            garbledText = title;
            mHolder.getText1().setText(album);
            mHolder.getText2().setText(artist);
            mHolder.getText3().setText(title);
            editAlbum.setText(album);
            editArtist.setText(artist);
            editTitle.setText(title);
            
            Hashtable<String, String> tbl1= new Hashtable<String, String>(), tbl2= new Hashtable<String, String>(); 
            Bitmap bmp = Funcs.getAlbumArt(this, targetId, R.drawable.albumart_mp_unknown, tbl1, tbl2);
            mHolder.getImage1().setImageBitmap(bmp);
        }
    }

    private void setupAlbumEdit() {
        findViewById(R.id.textTitle).setVisibility(View.GONE);
        editTitle.setVisibility(View.GONE);

        Hashtable<String, String> tbl = ContentsUtils.getAlbum(this,
                new String[] { AudioAlbum._ID, AudioAlbum.ALBUM,
                        AudioAlbum.ARTIST, AudioAlbum.ALBUM_ART }, editKey);

        albumKey = editKey;

        if (tbl != null) {
            targetId = Funcs.parseLong(tbl.get(AudioAlbum._ID));
            String album = tbl.get(AudioAlbum.ALBUM);
            String artist = tbl.get(AudioAlbum.ARTIST);
            String album_art = tbl.get(AudioAlbum.ALBUM_ART);
            garbledText = album;
            mHolder.getText1().setText(album);
            mHolder.getText2().setText(artist);
            editAlbum.setText(album);
            editArtist.setText(artist);
            Integer key = Funcs.getAlbumKey(album, artist);
            Bitmap bmp = mViewCache
                    .getImage(album, artist, album_art, new ImageObserverImpl(
                            mHandler, mViewCache, mHolder, key, 0));
            if (bmp != null) {
                mHolder.getImage1().setImageBitmap(bmp);
            } else {
                mHolder.getImage1().setImageBitmap(null);
            }
        }
    }

    @Override
    IMediaPlayerServiceCallback getCallBack() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    ViewCache getViewCache() {
        // TODO Auto-generated method stub
        return mViewCache;
    }

    @Override
    Handler getHandler() {
        // TODO Auto-generated method stub
        return mHandler;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imageAlbumArt) {
            if (albumKey != null) {
                Intent i = new Intent(this, AlbumartActivity.class);
                i.putExtra("album_key", albumKey);
                startActivity(i);
            } else if (artistKey != null) {
                Intent i = new Intent(this, AlbumartActivity.class);
                i.putExtra("artist_key", artistKey);
                startActivity(i);
            }
        } else if (v.getId() == R.id.btnGarbled) {
            EnocdeStringListDialog dlg = new EnocdeStringListDialog();
            Bundle args = new Bundle();
            args.putString(SystemConsts.KEY_SOURCESTR, garbledText);
            args.putLong(SystemConsts.KEY_SOURCEKEY, targetId);
            dlg.setArguments(args);
            dlg.setCallback(this);
            dlg.show(getSupportFragmentManager(), "encstring.dlg");
        } else if (v.getId() == R.id.btnBack) {
            setResult(RESULT_OK);
            finish();
        } else if (v.getId() == R.id.btnApply) {
            // 適用して終了
            apply();
            setResult(RESULT_OK);
            finish();
        }

    }
    
    @Override
    public void onEncodedStringSelect(String s, String enc, long id) {
        if (editType == ALBUM) {
            ContentValues value = new ContentValues();
            value.put(AudioMedia.ENCODING, enc);
            getContentResolver().update(
                    ContentUris.withAppendedId(MediaConsts.ALBUM_CONTENT_URI,
                            id), value, null, null);
            
            try {
                String album = Funcs.getEncodingString(mHolder.getText1().getText().toString(), enc);
                String artist = Funcs.getEncodingString(mHolder.getText2().getText().toString(), enc);
                
                mHolder.getText1().setText(album);
                mHolder.getText2().setText(artist);
                editAlbum.setText(album);
                editArtist.setText(artist);
            } catch (UnsupportedEncodingException e) {
            }
        }
        else if (editType == ARTIST) {
            ContentValues value = new ContentValues();
            value.put(AudioMedia.ENCODING, enc);
            getContentResolver().update(
                    ContentUris.withAppendedId(MediaConsts.ARTIST_CONTENT_URI,
                            id), value, null, null);
            
            try {
                String artist = Funcs.getEncodingString(mHolder.getText1().getText().toString(), enc);
                
                mHolder.getText1().setText(artist);
                editArtist.setText(artist);
            } catch (UnsupportedEncodingException e) {
            }
        }
        else if (editType == MEDIA) {
            ContentValues value = new ContentValues();
            value.put(AudioMedia.ENCODING, enc);
            getContentResolver().update(
                    ContentUris.withAppendedId(MediaConsts.MEDIA_CONTENT_URI,
                            id), value, null, null);
            
            try {
                String album = Funcs.getEncodingString(mHolder.getText1().getText().toString(), enc);
                String artist = Funcs.getEncodingString(mHolder.getText2().getText().toString(), enc);
                String title = Funcs.getEncodingString(mHolder.getText3().getText().toString(), enc);
                
                mHolder.getText1().setText(album);
                mHolder.getText2().setText(artist);
                mHolder.getText3().setText(title);
                editAlbum.setText(album);
                editArtist.setText(artist);
                editTitle.setText(title);
            } catch (UnsupportedEncodingException e) {
            }
        }
    }

    private void apply() {
        if (editType == ALBUM) {
            if(targetId >= 0){
                ContentValues value = new ContentValues();
                if(!editAlbum.getText().equals(mHolder.getText1().toString())){
                    value.put(AudioAlbum.ALBUM, editAlbum.getText().toString());
                }
                if(!editArtist.getText().equals(mHolder.getText2().toString())){
                    value.put(AudioAlbum.ARTIST, editArtist.getText().toString());
                }
                if(value.size()>0){
                    getContentResolver().update(
                            ContentUris.withAppendedId(MediaConsts.ALBUM_CONTENT_URI,
                                    targetId), value, null, null);
                }
            }
        } else if (editType == ARTIST) {
            if(targetId >= 0){
                ContentValues value = new ContentValues();
                if(!editArtist.getText().equals(mHolder.getText1().toString())){
                    value.put(AudioArtist.ARTIST, editArtist.getText().toString());
                }
                if(value.size()>0){
                    getContentResolver().update(
                            ContentUris.withAppendedId(MediaConsts.ARTIST_CONTENT_URI,
                                    targetId), value, null, null);
                }
            }
        } else if (editType == MEDIA) {            
            if(targetId >= 0){
                ContentValues value = new ContentValues();
                if(!editAlbum.getText().equals(mHolder.getText1().toString())){
                    value.put(AudioAlbum.ALBUM, editAlbum.getText().toString());
                }
                if(!editArtist.getText().equals(mHolder.getText2().toString())){
                    value.put(AudioAlbum.ARTIST, editArtist.getText().toString());
                }
                if(!editTitle.getText().equals(mHolder.getText3().toString())){
                    value.put(AudioAlbum.TITLE, editTitle.getText().toString());
                }
                if(value.size()>0){
                    getContentResolver().update(
                            ContentUris.withAppendedId(MediaConsts.MEDIA_CONTENT_URI,
                                    targetId), value, null, null);
                }
            }
        }
    }

    @Override
    public Fragment hideMenu() {
        // TODO Auto-generated method stub
        return null;
    }

}
